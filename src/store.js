import { configureStore } from '@reduxjs/toolkit'
import timelineReducer from './features/timeline/timeline-slice'

export const store = configureStore({
  reducer: {
    timeline: timelineReducer
  }
});

