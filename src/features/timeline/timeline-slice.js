import { createSlice } from '@reduxjs/toolkit'
import { v4 as uuidv4 } from 'uuid'

const initialState = {
  tracks: {
    'track-1': {
      boxes: {}
    },
    'track-2': {
      boxes: {}
    },
    'track-3': {
      boxes: {}
    }
  }
}

const timelineSlice = createSlice({
  name: 'timeline',
  initialState,
  reducers: {
    addBox(state, action) {
      const newId = uuidv4();
      const { trackId, box } = action.payload
      state.tracks[trackId].boxes[newId] = {
        id: newId,
        color: box.color,
        left: box.left,
        width: box.width
      }
    },
    moveBox(state, action) {
      const { from, to, boxId, left } = action.payload
      state.tracks[to].boxes[boxId] = state.tracks[from].boxes[boxId]
      state.tracks[to].boxes[boxId].left = left
      delete state.tracks[from].boxes[boxId]
    },
    updateBoxSize(state, action) {
      const { trackId, boxId, width } = action.payload
      state.tracks[trackId].boxes[boxId].width = width
    },
    updateBoxOffset(state, action) {
      const { trackId, boxId, left } = action.payload
      state.tracks[trackId].boxes[boxId].left = left
    }
  }
})

export const { addBox, moveBox, updateBoxSize, updateBoxOffset } = timelineSlice.actions
export default timelineSlice.reducer