import { useRef } from 'react'
import { useDrop } from 'react-dnd';
export function useLocalDrop(onDrop) {
  const ref = useRef();

  const [, dropTarget] = useDrop({
    accept: 'BOX',
    drop(item, monitor) {
      const offset = monitor.getSourceClientOffset();
      if (offset && ref.current) {
        const dropTargetXy = ref.current.getBoundingClientRect();
        onDrop(item, {
          x: offset.x - dropTargetXy.left,
          y: offset.y - dropTargetXy.top
        });
      }
    }
  });

  return elem => {
    ref.current = elem;
    dropTarget(ref);
  };
}