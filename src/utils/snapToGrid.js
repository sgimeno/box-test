export const snapToGrid = gap => x => {
  return Math.round(x / gap) * gap
}