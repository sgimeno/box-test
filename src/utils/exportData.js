import { MIN_GRID_GAP_X } from '../constants'

export function exportData(tracks) {
  const toArrayWithId = id => o => Object.keys(o).map(key => ({
    ...o[key],
    trackId: id
  }))

  const normalizeUnits = box => ({
    ...box,
    left: box.left / MIN_GRID_GAP_X,
    width: box.width / MIN_GRID_GAP_X
  })

  const result = Object.keys(tracks)
    .map(key => toArrayWithId(key)(tracks[key].boxes))
    .filter(boxes => boxes.length > 0)
    .reduce((acc, curr) => acc.concat(curr), [])
    .map(normalizeUnits)
    .sort((a, b) => a.left - b.left)

  return result
}