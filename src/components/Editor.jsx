import React from 'react'
import styled from 'styled-components'
import { useSelector, useDispatch } from 'react-redux'

import { exportData } from '../utils/exportData'
import { addBox, moveBox, updateBoxSize, updateBoxOffset } from '../features/timeline/timeline-slice'
import { DraggableBox } from './DraggableBox'
import { Box } from './Box'
import { TimelineTrack } from './TimelineTrack'

const INITIAL_BOXES = [
  {
    id: 'box-1',
    title: 'Box 1',
    color: 'purple',
    width: 200
  },
  {
    id: 'box-2',
    title: 'Box 2',
    color: 'teal',
    width: 200
  },
  {
    id: 'box-3',
    title: 'Box 3',
    color: 'pink',
    width: 200
  }
]

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 8px;
`

const BoxesList = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 200px);
  grid-gap: 8px;
  margin-bottom: 16px;
`
const Footer = styled.div`
  display: flex;
  margin-top: 16px;
`

const Button = styled.button`
  background-color: #6574CD;
  padding: 8px;
  color: white;
  border: none;
`

export const Editor = () => {
  const tracks = useSelector((state) => state.timeline.tracks)
  const dispatch = useDispatch()

  const trackActions = {
    addBox: (trackId, box) => dispatch(addBox({ trackId, box })),
    moveBox: (source, target, boxId, left) => dispatch(moveBox({ from: source, to: target, boxId, left })),
    updateBoxSize: (trackId, boxId, width) => dispatch(updateBoxSize({ trackId, boxId, width })),
    updateBoxOffset: (trackId, boxId, left) => dispatch(updateBoxOffset({ trackId, boxId, left }))
  }
  
  return (
    <Container>
      <BoxesList>
        {INITIAL_BOXES.map(box => (
          <DraggableBox key={box.id} item={box}>
            <Box title={box.title} color={box.color} width={box.width} />
          </DraggableBox>
        ))}
      </BoxesList>
      {
        Object.keys(tracks).map(key => (
          <TimelineTrack key={key} trackId={key} {...tracks[key]} {...trackActions} />
        ))
      }
    <Footer>
        <Button onClick={() => console.log(exportData(tracks))}>Export</Button>
    </Footer>
    </Container>
  )
}