import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  border-radius: 3px;
  box-sizing: border-box;
  border: 1px solid black;
  width: 100%;
  height: 60px;
  background-color: var(--${props => props.color});
`

export const Box = ({ title, color }) => {
  return (
    <Container color={color}>
      <h3>{ title }</h3>
    </Container>
  )
}
