import React, { useEffect } from 'react'
import styled from 'styled-components'
import { useDrag } from 'react-dnd'

const Container = styled.div`
  cursor: move;
  position: ${props => props.isInTimeline ? 'absolute': 'unset' };
  opacity: ${props => props.isDragging ? 0.5 : 1};
  transform : translate3d(${ props => props.left}px, 0px, 0);
`

export const DraggableBox = ({ children, item, isInTimeline }) => {
  const [{ isDragging }, drag] = useDrag(() => ({
    type: 'BOX',
    item: { ...item },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging()
    })
  }), [item])

  return (
    <Container ref={drag} left={item.left} isDragging={isDragging} isInTimeline={isInTimeline}>
      {children}
    </Container>
  )
}
