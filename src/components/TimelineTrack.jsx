import React from 'react'
import styled from 'styled-components'

import { MIN_GRID_GAP_X } from '../constants'
import { snapToGrid } from '../utils/snapToGrid'
import { useLocalDrop } from '../utils/useLocalDrop';
import { TimelineBox } from './TimelineBox';

const Container = styled.div`
  background-color: dimgray;
  width: 100%;
  height: 64px;
  display: flex;
  align-items: center;
  position: relative;
  overflow-x: hidden;
`

const snap = snapToGrid(MIN_GRID_GAP_X)

export const TimelineTrack = ({ trackId, boxes, addBox, moveBox, updateBoxSize, updateBoxOffset }) => {
  const drop = useLocalDrop((item, dropTargetXy) => {
    let left = dropTargetXy.x > 0 ? dropTargetXy.x : 0
    if (item.source === undefined || item.source === trackId) {
      if (boxes[item.id] === undefined ) {
        addBox(trackId, { ...item, left: snap(left) })
      } else  {
        updateBoxOffset(trackId, item.id, snap(left))
      }
    } else {
      moveBox(item.source, trackId, item.id, snap(left))
    }
    return undefined;
  })

  const handleResize = (boxId) => (width) => {
    updateBoxSize(trackId, boxId, snap(width))
  }
  return (
    <Container ref={drop}>
      {Object.keys(boxes).map(key => (
        <TimelineBox key={key} trackId={trackId} {...boxes[key]} onResize={handleResize(key)} />
      ))}
    </Container>
  )
}
