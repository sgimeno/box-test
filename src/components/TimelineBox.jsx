import React from 'react'
import { DraggableBox } from './DraggableBox'
import { ResizableBox } from './ResizableBox';
import { Box } from './Box';

export const TimelineBox = ({ trackId, id, color, left = 0, width, onResize }) => {
  return (
    <DraggableBox item={{ source: trackId, id, color, left, width }} isInTimeline>
      <ResizableBox width={width} onResize={onResize}>
        <Box color={color} />
      </ResizableBox>
    </DraggableBox>
  );
}
