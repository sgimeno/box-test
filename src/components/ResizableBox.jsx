import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  width: ${props => props.width}px;
  resize: horizontal;
  overflow: auto;
`

export const ResizableBox = ({ children, width, onResize }) => {
  const ref = React.createRef()
  const handleMouseMove = () => {
    const { width } = ref.current.getBoundingClientRect()
    onResize(width)
  }
  return (
    <Container ref={ref} width={width} onMouseMove={handleMouseMove}>
      {children}
    </Container>
  )
}