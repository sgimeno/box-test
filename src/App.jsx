import React from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { Editor } from './components/Editor'
import './App.css'

function App() {
  return (
    <div className="App">
      <DndProvider backend={HTML5Backend}>
        <Editor />
      </DndProvider>
    </div>
  )
}

export default App
