## Setup instructions

### Requirements

 + NodeJS v12+

### Install

```sh
$ npm install
```

### Build and run

To build and run the application

```sh
$ npm run build
$ npm run serve
```
